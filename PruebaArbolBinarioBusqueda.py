'''
Created on 19 nov. 2020

@author: Cesar
'''




class Nodo:
    def __init__(self, dato):
        if dato is None:
            self.__dato = (-1)
        else:
            self.__dato = dato
        self.__izquierdo = None
        self.__derecho = None
        
    def getDato(self):
        return self.__dato
    def setDato(self, dato):
        self.__dato=dato
    def getIzquierdo(self):
        return self.__izquierdo
    def setIzquierdo(self, izquierdo):
        self.__izquierdo=izquierdo
    def getDerecho(self):
        return self.__derecho
    def setDerecho(self, derecho):
        self.__derecho=derecho
    
    def __str__(self):
        return ("Nodo [Izquierdo=" + str(self.getIzquierdo()) + ", dato=" + str(self.getDato())+ ", nodoSiguiente=" + str(self.getDerecho()) + "]")
    
class ArbolBinarioBusqueda:
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r

    
    
    
    
    
    def __init__(self):
        self.raiz = None
    
    def agregar(self, dato):
        nuevoNodo = Nodo(dato)
        
        if(self.raiz==None):
            self.raiz = nuevoNodo
        else:
            aux = self.raiz
            nodoAnterior= None
            
            while(aux!=None):
                nodoAnterior = aux
                
                if(dato>=aux.getDato()):
                    aux = aux.getIzquierdo()
                    if(aux==None):
                        nodoAnterior.setIzquierdo(nuevoNodo)
                else:
                    aux = aux.getDerecho()
                    if(aux==None):
                        nodoAnterior.setDerecho(nuevoNodo)
    def desplazar(self, nodo):
        anterior = nodo
        ret = nodo
        aux = nodo.getDerecho()
        
        while(aux!=None):
            anterior = ret
            ret = aux
            aux= aux.getIzquierdo()
        
        if(ret!=nodo.getDerecho()):
            anterior.setIzquierdo(ret.getDerecho())
            ret.setIzquierdo(nodo.getDerecho())
        
        return ret
    def eliminar(self, dato):
        if (self.raiz!=None):
            izq=0
            nodoAnterior = self.raiz
            aux = self.raiz
            
            while((aux!=None) and (aux.getDato()!=dato)):
                nodoAnterior = aux
                    
                if(aux.getDato()<=dato):
                    izq=1
                    aux = aux.getIzquierdo()
                else:
                    izq=0
                    aux = aux.getDerecho()
                        
            if (aux==None):
                print("no se encontro el dato")
                return False
            else:
                print("se encontro el dato")
            
            
            if(aux.getIzquierdo()==None and aux.getDerecho()==None):
                if(aux==self.raiz):
                    self.raiz = None
                elif(izq==1):
                    nodoAnterior.setIzquierdo(None)
                else:
                    nodoAnterior.setDerecho(None)
            elif(aux.getIzquierdo()==None):
                if(aux==self.raiz):
                    self.raiz = aux.getDerecho()
                elif(izq==1):
                    nodoAnterior.setIzquierdo(aux.getDerecho())
                else:
                    nodoAnterior.setDerecho(aux.getDerecho())
                
            elif(aux.getDerecho()==None):
                if(aux==self.raiz):
                    self.raiz = aux.getIzquierdo()
                elif(izq==1):
                    nodoAnterior.setIzquierdo(aux.getIzquierdo())
                else:
                    nodoAnterior.setDerecho(aux.getIzquierdo())
                
            else:
                desplace = self.desplazar(aux)
                if(aux==self.raiz):
                    self.raiz = desplace
                elif(izq==1):
                    nodoAnterior.setIzquierdo(desplace)
                else:
                    nodoAnterior.setDerecho(desplace)
                
                desplace.setIzquierdo(aux.getIzquierdo())
            
            return True
        else:
            print("Ups... Arbol Vacio")
            return False
    def recorridoPreorden(self, raiz):
        if(raiz!=None):
            print(str(raiz.getDato()),end=" => ")
            self.recorridoPreorden(raiz.getIzquierdo())
            self.recorridoPreorden(raiz.getDerecho())
    def recorridoEnorden(self, raiz):
        if(raiz!=None):
            self.recorridoEnorden(raiz.getIzquierdo())
            print(str(raiz.getDato()),end=" => ")
            self.recorridoEnorden(raiz.getDerecho())
            
    def recorridoPostorden(self, raiz):
        if(raiz!=None):
            self.recorridoPostorden(raiz.getIzquierdo())
            self.recorridoPostorden(raiz.getDerecho())
            print(str(raiz.getDato()),end=" => ")
    def mostrarDatoMayor(self):
        if(self.raiz==None):
            print("Ups... Arbol Vacio")
        else:
            aux = self.raiz
            nodoAnterior=None
            
            while(aux!=None):
                nodoAnterior = aux
                aux = aux.getIzquierdo()
                if(aux==None):
                    print("El Dato MAYOR es: "+str(nodoAnterior.getDato()))
    def mostrarDatoMenor(self):
        if(self.raiz==None):
            print("Ups... Arbol Vacio")
        else:
            aux = self.raiz
            nodoAnterior=None
            
            while(aux!=None):
                nodoAnterior = aux
                aux = aux.getDerecho()
                if(aux==None):
                    print("El Dato MENOR es: "+str(nodoAnterior.getDato()))
    def buscarDato(self, nodo, dato, en):
        if(nodo!=None):
            if(dato==nodo.getDato()):
                en+=1
            en+=self.buscarDato(nodo.getIzquierdo(),dato,en)
            en+=self.buscarDato(nodo.getDerecho(),dato,en)
        
        return en
    
    


opcion=0
dato=0
numero=0
abb = ArbolBinarioBusqueda()

while True:
   
    print("==================== MENU ====================")
    print("Digite 1 para Agregar un nodo al Arbol")
    print("Digite 2 para Eliminar un nodo al Arbol")
    print("Digite 3 para mostrar el Recorrido del arbol PREORDEN")
    print("Dihite 4 para mostrar el recorrido del arbol INORDEN")
    print("Digite 5 para mostrar el recorrido del arbol POSTORDEN ")
    print("Digite 6 para mostrar el dato MAYOR")
    print("Digite 7 para mostrar el dato MENOR")
    print("Digite 8 para buscar un Dato ")
    print("Digite 9 para ***SALIR***")
    opcion=abb.correcion()
    
    if(opcion==1):
        print("Ingresa el dato a añadir :")
        dato = abb.correcion()
        abb.agregar(dato)
    elif(opcion==2):
        print("Ingresa el dato a eliminar:")
        dato = abb.correcion()
        abb.eliminar(dato)
    elif(opcion==3):
        abb.recorridoPreorden(abb.raiz)  
    elif(opcion==4):
        abb.recorridoEnorden(abb.raiz)
    elif(opcion==5):
        abb.recorridoPostorden(abb.raiz)
    elif(opcion==6):
        abb.mostrarDatoMayor()
    elif(opcion==7):
        abb.mostrarDatoMenor()   
    elif(opcion==8):
        print("Ingrese el dato a buscar: ")
        dato = abb.correcion()
        encontrado = abb.buscarDato(abb.raiz, dato, 0)
        if(encontrado>0):
            print("El dato ha sido encontrado")
        else:
            print("El dato no existe")
    elif(opcion==9):
        print("Gracias por usar el programa")
        break
    else:
        print("Opcion no valida")



         
        